package com.example.furkan.deneme1;

import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView txtArtis;
    private Button btnArtis;
    private Button btnAzalt;
    private RadioButton rdyX1, rdyX5, rdyX10;
    private int i = 0;

    private void init()
    {
        txtArtis = (TextView) this.findViewById(R.id.txtArtis);
        btnArtis = (Button) this.findViewById(R.id.btnArtis);
        btnAzalt = (Button) this.findViewById(R.id.btnAzalt);
        rdyX1 = (RadioButton) this.findViewById(R.id.rdyX1);
        rdyX5 = (RadioButton) this.findViewById(R.id.rdyX5);
        rdyX10 = (RadioButton) this.findViewById(R.id.rdyX10);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        artir(txtArtis);
        azalt(txtArtis);
    }

    public void artir(View v)
    {
        if(rdyX1.isChecked())
        {
            x1Click(v);
            i++;
        }
        else if(rdyX5.isChecked()) {
            i = i + 5;
            x5Click(v);
        }
        else if(rdyX10.isChecked())
        {
            i = i + 10;
            x10Click(v);

        }


        txtArtis.setText(String.format("Sayı: %d",i));
    }

    public void azalt(View v)
    {
        if(rdyX1.isChecked())
        {
            i--;
            x1Click(v);
        }
        else if(rdyX5.isChecked())
        {
            i = i-5;
            x5Click(v);
        }
        else if(rdyX10.isChecked())
        {
            i = i - 10;
            x10Click(v);

        }

        txtArtis.setText(String.format("Sayı: %d",i));
    }

    public void x1Click(View v)
    {
        rdyX5.setChecked(false);
        rdyX10.setChecked(false);
    }

    public void x5Click(View v)
    {
        rdyX1.setChecked(false);
        rdyX10.setChecked(false);
    }

    public void x10Click(View v)
    {
        rdyX5.setChecked(false);
        rdyX1.setChecked(false);
    }
}
